﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    private const int MIN_TILE_DISTANCE = -1;
    private const int MAX_TILE_DISTANCE = 2;
    private const int CORNER_TILE_DISTANCE = 1;
    private const float FIRST_TIME_DELAY = 1f;
    private const float DISTANCE_BETWEEN_TILES = 110f;

    [Range(min: 30, max: 50)]
    [SerializeField] private int ShuffleSteps;
    [Range(min: 2, max: 8)]
    [SerializeField] private int m_gamefieldSideSize = 4;
    [SerializeField] private GameObject m_tilePrefab;
    [SerializeField] private Transform m_tilesParentTr;
    [SerializeField] private PuzzleView m_view;
    private RectTransform m_tilesRectTr;

    private List<Tile> m_tilesList;
    private Vector2Int m_emptyTilePosition;

    private void Awake()
    {
        m_tilesList = new List<Tile>();
        m_view.RestartClicked = Shuffle;
        m_tilesRectTr = m_tilesParentTr.GetComponent<RectTransform>();
        GenerateGameField();
    }

    private void GenerateGameField()
    {
        var generalSize = GetTilesCount(m_gamefieldSideSize);
        var tilesParentX = -(m_gamefieldSideSize * DISTANCE_BETWEEN_TILES) / 2f;
        var tilesParentY = (m_gamefieldSideSize * DISTANCE_BETWEEN_TILES) / 2f;

        m_tilesRectTr.anchoredPosition = new Vector3(tilesParentX,tilesParentY,0f);

        for (int i = 0; i < generalSize; i++)
        {
            var go = Instantiate(m_tilePrefab, m_tilesParentTr);
            var tile = go.GetComponent<Tile>();
            var tileNumber = i + 1;
            var tilePosition = GetTilePosition(i);
            var rectTransfrom = tile.GetComponent<RectTransform>();
            rectTransfrom.anchoredPosition = GetTileLocalPosition(i); 
            tile.Init(tileNumber, tilePosition);
            tile.TileSwiped = CheckCanMoveTile;
            m_tilesList.Add(tile);
        }

        m_emptyTilePosition = new Vector2Int(m_gamefieldSideSize - 1, m_gamefieldSideSize - 1);
        Shuffle();
    }

  
    private void Shuffle()
    {
        for (int i = 0; i < ShuffleSteps; i++)
        {
            var nextTile = GetRandomTileNearEmpty();
            var pos = nextTile.TilePosition;
            nextTile.MoveTileImmidiatelly(m_emptyTilePosition);
            m_emptyTilePosition = pos;
        }
    }


    private Tile GetRandomTileNearEmpty()
    {
        var neighbourTiles = new List<Tile>();
        //Get All tiles near the empty tile
        for (int i = MIN_TILE_DISTANCE; i < MAX_TILE_DISTANCE; i++)
        {
            for (int j = MIN_TILE_DISTANCE; j < MAX_TILE_DISTANCE; j++)
            {
                var x = m_emptyTilePosition.x - i;
                var y = m_emptyTilePosition.y - j;
                var neighbourPos = new Vector2Int(x,y);
                var neighbour = m_tilesList.Find(_=>_.TilePosition == neighbourPos);

                var delta = new Vector2Int(Mathf.Abs(m_emptyTilePosition.x - neighbourPos.x), Mathf.Abs(m_emptyTilePosition.y - neighbourPos.y));
                if (delta.x == CORNER_TILE_DISTANCE && delta.y == CORNER_TILE_DISTANCE)
                {
                    continue;
                }

                if (neighbour != null)
                {
                    neighbourTiles.Add(neighbour);
                }
            }
        }
        var tile = neighbourTiles[Random.Range(0, neighbourTiles.Count)];
       
        return tile;
    }

    private void CheckCanMoveTile(Tile tile, SwipeDirection swipeDirection)
    {
        var canMove = false;
        var position = tile.TilePosition;
        var isXEqual = position.x == m_emptyTilePosition.x;
        var isYEqual = position.y == m_emptyTilePosition.y;
        var isEmptyOnRight = m_emptyTilePosition.x - position.x == 1;
        var isEmptyOnLeft = position.x - m_emptyTilePosition.x == 1;
        var isEmptyOnUp = position.y - m_emptyTilePosition.y == 1;
        var isEmptyOnDown = m_emptyTilePosition.y - position.y == 1;

        switch (swipeDirection)
        {
            case SwipeDirection.Down:
                canMove = isXEqual && isEmptyOnDown;
                if (canMove)
                {
                    tile.TilePosition.y++;
                }
                break;
            case SwipeDirection.Up:
                canMove = isXEqual && isEmptyOnUp;
                if (canMove)
                {
                    tile.TilePosition.y--;
                }
                break;
            case SwipeDirection.Left:
                canMove = isYEqual && isEmptyOnLeft;
                if (canMove)
                {
                    tile.TilePosition.x--;
                }
                break;
            case SwipeDirection.Right:
                canMove = isYEqual && isEmptyOnRight;
                if (canMove)
                {
                    tile.TilePosition.x++;
                }
                break;
        }

        if (canMove)
        {
            m_emptyTilePosition = position;
            tile.MoveTile(swipeDirection,CheckIsWin);
        }

    }

    private void CheckIsWin()
    {
        var isWin = true;
        var generalSize = GetTilesCount(m_gamefieldSideSize);

        for (int i = 0; i < generalSize; i++)
        {
            var currentTile = m_tilesList[i];
            var winPosition = GetTilePosition(i);
            if (currentTile.TilePosition != winPosition)
            {
                isWin = false;
                break;
            }
        }

        if (isWin)
        {
            m_view.SetGridActive(false);
            m_view.SetWinTextActive(isWin);
            Shuffle();
        }
    }

    private Vector2Int GetTilePosition(int index)
    {
        var x = index / m_gamefieldSideSize > 0 ? index % m_gamefieldSideSize : index;
        var y = Mathf.RoundToInt(index / m_gamefieldSideSize);
        var tilePosition = new Vector2Int(x, y);
        return tilePosition;
    }

    private Vector3 GetTileLocalPosition(int index)
    {
        var tilePos = GetTilePosition(index);

        var x = tilePos.x * DISTANCE_BETWEEN_TILES;

        var y = tilePos.y * -DISTANCE_BETWEEN_TILES;
        var tilePosition = new Vector3(x, y,0f);
        return tilePosition;
    }

    private int GetTilesCount(int sideSize)
    {
        return sideSize * sideSize - 1;
    }


}
