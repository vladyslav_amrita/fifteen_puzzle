﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PuzzleView : MonoBehaviour 
{
    private const float CANVAS_GROUP_ACTIVE_ALPHA = 1f;
    public Action RestartClicked = delegate { }; 
    [SerializeField] private GameObject m_winTextGo;
    [SerializeField] private CanvasGroup m_canvasGroup;

    public void InitView(int sideLength)
    {
        SetGridActive(false);
    }

    public void SetGridActive(bool isActive)
    {
        var alpha = isActive ? CANVAS_GROUP_ACTIVE_ALPHA : 0f;
        m_canvasGroup.alpha = alpha;
    }

    public void SetWinTextActive(bool isActive)
    {
        m_winTextGo.SetActive(isActive);
    }

    //Restart button onClick handler
    public void OnRestart()
    {
        SetGridActive(true);
        SetWinTextActive(false);
        RestartClicked();
    }

}
