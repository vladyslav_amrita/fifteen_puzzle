﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;

public class Tile : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public Action<Tile, SwipeDirection> TileSwiped = delegate { };
    [HideInInspector] public SwipeDirection CurrentSwipeDirection;
    public Vector2Int TilePosition;
    public int TileNumber;

    private const float SWIPE_DISTANCE = 0.5f;
    private const float MOVE_DISTANCE = 110f;
    private const float MOVE_TIME = 0.2f;

    [SerializeField] private Text m_text;

    private Transform m_cachedTransform;
    private RectTransform m_rectTr;
    private Vector2 m_firstPressPos;
    private Vector2 m_secondPressPos;
    private Vector2 m_currentSwipe;

    private void Awake()
    {
        m_cachedTransform = transform;
        m_rectTr = GetComponent<RectTransform>();
    }

    public void Init(int tileNumbrer, Vector2Int tilePosition)
    {
        TilePosition = tilePosition;
        TileNumber = tileNumbrer;
        m_text.text = tileNumbrer.ToString();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        m_firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        m_secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        m_currentSwipe = new Vector2(m_secondPressPos.x - m_firstPressPos.x, m_secondPressPos.y - m_firstPressPos.y);
        m_currentSwipe.Normalize();

        if (m_currentSwipe.y > 0 && m_currentSwipe.x > -SWIPE_DISTANCE && m_currentSwipe.x < SWIPE_DISTANCE)
        {
            CurrentSwipeDirection = SwipeDirection.Up;
        }
        if (m_currentSwipe.y < 0 && m_currentSwipe.x > -SWIPE_DISTANCE && m_currentSwipe.x < SWIPE_DISTANCE)
        {
            CurrentSwipeDirection = SwipeDirection.Down;
        }
        if (m_currentSwipe.x < 0 && m_currentSwipe.y > -SWIPE_DISTANCE && m_currentSwipe.y < SWIPE_DISTANCE)
        {
            CurrentSwipeDirection = SwipeDirection.Left;
        }
        if (m_currentSwipe.x > 0 && m_currentSwipe.y > -SWIPE_DISTANCE && m_currentSwipe.y < SWIPE_DISTANCE)
        {
            CurrentSwipeDirection = SwipeDirection.Right;
        }

        TileSwiped(this,CurrentSwipeDirection);

    }

    //Move tile for shuffling
    public void MoveTileImmidiatelly(Vector2Int tilePosition)
    {
        var isHorizontal = tilePosition.y == TilePosition.y;
        var localTilePosition = m_rectTr.anchoredPosition;
        
        var position = isHorizontal ?
            new Vector3(localTilePosition.x + (tilePosition.x - TilePosition.x) * MOVE_DISTANCE, localTilePosition.y, 0f) :
            new Vector3(localTilePosition.x, localTilePosition.y + (TilePosition.y - tilePosition.y) * MOVE_DISTANCE, 0f);

        m_rectTr.anchoredPosition = position;
        TilePosition = tilePosition;
    }

    //Move tile in game
    public void MoveTile(SwipeDirection direction, Action onComplete)
    {
        var isHorizontal = direction == SwipeDirection.Right || direction == SwipeDirection.Left;
        var posMoveTo = direction == SwipeDirection.Down || direction == SwipeDirection.Left ? MOVE_DISTANCE * -1 : MOVE_DISTANCE;

        if (isHorizontal)
        {
            posMoveTo = m_rectTr.anchoredPosition.x + posMoveTo;
            m_rectTr.DOAnchorPosX(posMoveTo, MOVE_TIME).OnComplete(()=> onComplete());
        }
        else
        {
            posMoveTo = m_rectTr.anchoredPosition.y + posMoveTo;
            m_rectTr.DOAnchorPosY(posMoveTo, MOVE_TIME).OnComplete(() => onComplete());
        }

    }
}
